# README #

 

### What is this repository for? ###

* This repo showcases various demo programs for accessing the Rest API's found on the MCW https://cis.ctsi.mcw.edu/ web site. 

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

 

### Who do I talk to? ###

* If you have code to contrbute or questions please contact ctsi (dot ) biomedicalinformatics ( at ) mcw.edu
* Also Join us on Gitter at  https://gitter.im/text-deid/Lobby 