import cx_Oracle
import psycopg2
import psycopg2.extras
import json
import logging


def load_config(config_file="config/config.json"):
    with open(config_file) as f:
        config_dict = json.load(f)
    return config_dict


#
# Return the connection to be used during this pipeline
# Only place we hard code the Host and DB.
# TODO: Should not have this hard coded , but rather selectable via a command line option.
def get_connection(db="default", config=load_config()):
    if (db == "default"):
        dbhost = config["active_db"]
        dbschema = config["active_schema"]
        return get_connection_local(dbhost, dbschema, config)
    elif (db == "flint"):
        dbhost = config["active_db_flint"]
        dbschema = config["active_schema_flint"]
        return get_connection_local_pg(dbhost, dbschema, config)
    elif (db == "dbo_clarity"):
        dbhost = config["active_db_dbo_clarity"]
        dbschema = config["active_schema_db_dbo_clarity"]
        return get_connection_local(dbhost, dbschema, config)


def get_connection_local(host, db, config):
    connection_params = config[host][db]
    dsn_tns = cx_Oracle.makedsn(host=connection_params["host"],
                                port=connection_params["port"],
                                sid=connection_params["SID"])

    connection = cx_Oracle.connect(user=connection_params["user"],
                                   password=connection_params["password"],
                                   dsn=dsn_tns)
    return connection

def get_connection_local_pg(host, db, config):
    connection_params = config[host][db]
    conn_string = "host='"+ connection_params["host"] + "' dbname='" + connection_params["db"] + "' user='" + connection_params["user"] + "' password='" + connection_params["password"]+ "'"
    # print(conn_string)
    conn = psycopg2.connect(conn_string)

    # cursor = conn.cursor()
    # print(cursor)
    # conn.cursor will return a cursor object, you can use this cursor to perform queries

    return conn
