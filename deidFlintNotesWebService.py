#!python

import util.db_connection as db
import click
import psycopg2
import psycopg2.extras
import requests, json
import re


@click.command()
@click.option('--watson_query_id', required=True, help='Watson Query ID to deid the notes for .')
@click.option('--note_type', required=False, help='Watson Note Type to deid or if not specified do them all ')
# Main method , NOTE THis script will die it it encounters data like "\ultra;" in the text. 




def main(watson_query_id , note_type ):
    """ Program to read data from Postgres database , hit the de-id web service with it and update the returned deid
	text in postgres """

    print("Started program for query " + watson_query_id)

    conn = db.get_connection(db="flint")
    cur_flint = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

    conn1 = db.get_connection(db="flint")
    cur_flint1 = conn1.cursor(cursor_factory=psycopg2.extras.DictCursor)


    url = "https://cis.ctsi.mcw.edu/deid/deidservice"
    headers = {'content-type': 'application/json', 'Accept-Charset': 'UTF-8'}

    sqlToUpdate = """ select pn.id, p.first_name || ' ' || p.last_name as name , p.date_shift, pn.note
        from  flint_patientnotes pn 
        join flint_patient p on pn.patient_id = p.id 
        where  p.query_id = %s and note_deid is null """

    cur_flint.execute(sqlToUpdate, ( watson_query_id,) )

    for line in cur_flint.fetchall():
        id = line[0]
        patientName = line[1]
        dateShift=line[2]
        text = line[3]
        stringToSend = text.replace('\\',"")
        json_data = {}
        json_data["name"] = patientName
        json_data["dateoffset"] = dateShift
        json_data["recordlist"] = ( stringToSend , )
        print("\n")
        print("LINE: processing id {} : {}".format(id, json.dumps(json_data)))
        r = requests.post(url, data=json.dumps(json_data), headers=headers)
        resultingJson =   r.json()

        print("\n")
        deid_note_text = (resultingJson["deidlist"][0])
        print ( "Received:  {} ".format(deid_note_text))

        # Update database
        print( "update flint_patientnotes  where id  = {}".format ( id ))
        cur_flint1.execute ("""update flint_patientnotes set note_deid = %s where id  = %s;""", ( deid_note_text, id ))
        conn1.commit()


    conn.commit()
    conn1.commit()
    print("deidFlintNotesWebService done")



if __name__ == '__main__':
    main()
